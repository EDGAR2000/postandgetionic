import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient,HttpHeaders} from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http:HttpClient) { }
  
savedata(dataToSend){
  var url="http://localhost/APIs/AltaUsuario.php";
  return this.http.post(url,dataToSend,
  {headers:new HttpHeaders(
    {"content-type":"application/json"})});
}
}
