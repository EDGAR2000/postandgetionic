import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import {HttpHeaders} from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class ApisService {
  constructor(private  httpclient:HttpClient) {  }
// este jala el json con los datos
  obtenerDatos(){
    return this.httpclient.get('http://localhost/APIs/ObtenerUsuarios.php')
  }
 
}
